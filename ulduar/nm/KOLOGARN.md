## Kologarn



![Kologarn](https://www.warcrafttavern.com/wp-content/uploads/2022/10/Arms-3-2048x1152.jpg)

### Introduction

Attention, ici, le combat se fait sur un torse ainsi que les deux bras du boss (comme Der Richter pour celles et ceux qui ont déjà fait FF10...)

Autre aspect à retenir (surtout pour les tanks et cac) : vous pouvez tomber de la surface de combat (comme lors du combat contre Arthas - dans quelques mois...) et donc mourir inutilement.

--------



### Capacités

***Boss***

- HP: 20.9 million (25) / 4.88 million (10l)

- Frappe de haut en bas - Toutes les 10 à 15 secondes, attaque physique en mélee sur le MT qui ajoute Broie d'armure (décroit également l'armure de 25% pendant 30 secondes) sur la cible ce qui oblige à faire tourner le MT..

- Frappe de haut en bas, à une main: Quand Kologarn perd un bras, Frappe de haut en bas est remplacé par ce coup moins puissant mais avec toujours la même diminution d'armure.

- Souffle pétrifiant - Si aucun joueur n'est à distance de mélee, inflige de 18750 à 21250 dégâts nature toutes les secondes et augmente les dégâts pris de 20% pendant 8 secondes (Peau Fragile - augmente les dégâts pris de 20% pendant 8 secondes, les effets s'ajoutent lorsque la cible le subit plusieurs fois).

- Rayon de l'oeil focalisé - Inflige 3700 à 4300 dégâts nature aux ennemis dans les 3 mètres. Rayon produit par les yeux qui suit une cible similaire à Felmyst ( et je sais que ça vous rappelle de bons souvenirs)

  

  

  ***Bras gauche***

  -  HP: 5.2 million (25) / 1.26 million (10)
  - Onde de choc : toutes les 25 secondes environ, dégâts sur le raid de 12k dégâts nature.
  - Quand le bras gauche meurt, les points de vie de Kologarn diminuent de 15% (environ 3.5 million HP), 4 adds popent qui font 4k de dégâts en mélée et utilise Nova de pierre , un dégât en zone. 
  - Ces adds peuvent être stun et le bras repop en environ 30 secondes.



***Bras droit***



-  HP: 5.2 million (25) / 1.26 million (10)

- Poigne de pierre - Rend de la vie au bras droit de Kologarn en prenant celle d'une cible au hasard qui devient étourdie et qui subit 3700 à 4300 dégâts physique toutes les secondes jusqu'à 100000 dégâts.
- Quand le bras droit meurt, les points de vie de Kologarn diminuent de 15% (environ 3.5 million HP), 4 adds popent qui font 4k de dégâts en mélée et utilise Nova de pierre, un dégât en zone. 
- Ces adds peuvent être stun et le bras repop en environ 30 secondes.

----------



### Stratégie

#### Positionnement

Le raid doit être éparpiller autour de la plateforme devant Kologarn, tout en laissant assez de place pour maneuvrer lors du Rayon de l'oeil focalisé. 

Le Rayon de l'oeil focalisé poursuivant le joueur, la meilleur solution pour éviter est de tracer tout droit tandis que le raid s'écarte.

![placement](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Kologarn-Both-Arms-No-Rubble-2048x1152.png)

Dès que les add pop, on bascule comme ceci :

![positionnement_add](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Kologarn-One-Arm-Rubble-2048x1152.png)



Pour le rayon : ![rayon](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Kologarn-Both-Arms-Eye-Laser-2048x1152.png)

#### Les bras

Les bras ont 2 capacités primaires : Poigne de pierre (Bras Droit) et Onde de choc (Bras Gauche). 

Onde de choc ne présente qu'un danger minimal et un seul heal doit pouvoir couvrir les dégâts infligés par cette attaque. En conséquence, la plupart des guildes n'attaque pas le bras gauche.

 Le bras droite présente un réal danger à cause de la Poigne de pierre, qui fait de gros dégâts sur un joueur, il faut infliger 10000 points de dégâts au bras pour stopper l'attaque. 

Il va régulièrement prendre 3 personnes dans sa main en leur infligeant des dégats (gros heal nécessaire sur ces 3 joueurs). Au bout d'une dizaine de secondes il relâche les joueurs en les fracassant un peu violemment contre le sol (oui oui, ça OS). La seule solution est de dps très vite le bras de façon à le "tuer" avant qu'il ne balance les joueurs emprisonnés.

La meilleur stratégie est donc d'ignorer le bras gauche et de tuer le bras droite dès qu'il est en place.

#### Healer

Bien que Kologarn possède un enragé, le combat n'est pas une course au dps. Néanmoins, les dégâts infligés par Kologarn sont légérement supérieure aux autres boss d'Ulduar. Peut-être voir un passage à 5 heals sur ce fight.

#### Tank

Le boss attaque lentement mais fait de gros dégâts, surtout si vous avez le debuff Peau Fragile.

 2 tanks suffisent ici sur Kologarn : on fera une rotation entre ces deux tanks : le 2ème doit prendre la place du première dès que le premier possède 2 stack de Peau Fragile (à la manière de Gluth à Naxxramas). 

Si jamais par hasard le 2ème tank se prend la Poigne de pierre, il faudra claquer les cd du MT.

#### Dps

Les dps cac sont désavantagé sur ce boss. Si vous avez bien suivi, il n'est pas possible de se placer derrière le boss, ce qui va entrainer des parades et donc une légère baisse de dps.

-------



### Hard mode

Pas de HM ici.



-------------

### Vidéos

- https://youtu.be/kIqyG3msJWA?t=1306
- https://www.youtube.com/watch?v=gs6L9Vvdkv8 (pov tank)
- https://youtu.be/3uwg7WgX7mY?t=93

