## Hodir

![Hodir](https://www.warcrafttavern.com/wp-content/uploads/2022/10/Ice-3-2048x1152.jpg)



### Introduction

Le combat normal contre Hodir est un test de notre capacité à éviter les dégâts et les effets de l'environnement. Autant dire que ce combat va jeter...un froid.

La seule chose necessaire est d'avoir de gros heal pour ce combat car forcément les singes vont fatalement slacker dans les AOE. 

Pour ce combat : un seul tank, 4 heals, le reste en dps. 

Par contre, les hunts verront leur dps diminuer sur ce combat, à cause des buff des PNJ.

Si on se rend compter que les gens sont mauvais pour éviter les aoe ou prennent trop de dégats, on pourra demander de ressortir le stuff RG utilisé sur Saphhiron à Naxxramas. 

------

### Capacités

- PV :  32,9 millions de points de vie

- Berserk de 10 minutes, on est large (sauf si on veut le battre en HM, voir plus bas)

- Froid mordant : Inflige des dégâts périodiques croissants aux ennemis stationnaires. Le déplacement diminue cet effet. Les dégâts infligés sont équivalent a ce petite calcul :  

  

  ```mathematica
    100*(2^(Nombre de stacks de Froid Mordant)
  ```

    avec un maximum de 8 stacks (25600 points de dégâts). Il tick chaque seconde.

- Coups gelés : Dégâts physiques réduits de 70%, mais les attaques infligent 31062 points de dégâts de Givre supplémentaires et tous les ennemis subissent 4000 points de dégâts de Givre toutes les 2 sec.

- Gel instantané : Emprisonne tous les joueurs de la zone sous un bloc de glace d'environ 44k hp (35k en 10). Si la cible est Freeze, elle est instantanément tuée. Le cooldown est d'environ 45 secondes et le temps de cast est assez long (9 secondes), ce qui permet de se placer au bon endroit pour ne pas subir l'effet du Flash Freeze (voir plus bas).
- Avalanche : Blocs de glace qui tombent du plafond. Fait 12k de dégâts et repousse (knock back) à 20 mètres. Rester sur les blocs restant pendant un flash freeze vous empêche de rester emprisonné dans un glaçon.
- Gel : Inflige des dégâts froid (environ 5k en 25) sur une zone, immobilisants tous les personnages pendant 10 secondes

-----

### Les copains copines

La rencontre commence avec huit PNJ congéles en blocs de glace avec le sort Gel instantané devant Hodir. 

Ces alliés peuvent être libérés par la rupture de leurs bloc de glace, les pnj sont constituté de deux chaque type : 

- 2 prêtres : Prêtre de bataille Eliza and Gina

  - Châtiment
    Inflige des dégâts du Sacré à un ennemi.
  - Dissipation de la magie
    Dissipe la magie sur un allié, annulant 1 sort néfaste.
  - Soins supérieurs
    Fait appel à la magie du Sacré pour soigner un allié.

  

- 2 poulet lasers : Tor and Kar Greycloud

  - Colère
    Frappe un ennemi avec un éclair et lui inflige des dégâts de Nature.

  - Puit de Lumière

    Pose un puit de lumière qui buff les joueurs dedans.

![puit_lumière](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Hodir-Starlight-2048x1152.jpg)





- 2 mages feu : Amira and Veesha Blazeweaver 
  - Boule de feu
    Projette une boule ardente qui inflige 3570 to 4830 points de dégâts de Feu et 1800 points de dégâts de Feu supplémentaires en 6 sec.
  - Fonte de la glace
    Tente de faire fondre un Gel instantané proche en infligeant un montant de dégâts de Feu égal à 100% de son total de points de vie en 10 sec.
  - Invocation d'un feu douillet
    Invoque un feu douillet qui réchauffe les unités alliées proches. Le feu douillet peut être éteint par une chute de glace ou par Gel instantané.



![feu](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Hodir-Bonfires-2048x1152.jpg)



- 2 chaman élém : Spiritwalkers Tara and Yona

  - Nuage d'orage
    Invoque un nuage d'orage chargé d'éclairs sur un personnage-joueur allié aléatoire. Tant qu'il est entouré par ce nuage, un maximum de 4 unités alliées touchées par ce personnage sont frappées par un éclair et reçoivent la Puissance de la tempête, ce qui augmente les dégâts de leurs coups critiques de 135% pendant 30 sec
  - Explosion de lave
    Vous lancez de la lave en fusion sur la cible, lui infligeant 4250 to 5750 points de dégâts de Feu.

  

- Chacun de ces pnj attaque Hodir, aide le raid et donne des buffs au raid. Mais étant de bons fruits, les PNJ ne savent pas comment éviter le sort Gel instantané et doivent être libéré de leurs blocs de glace après chaque Gel Instantané.

------



### Stratégie 

Le début du combat est très simple :

Le tank doit avoir Hodir de pull grâce à un détournement, pour le placer au centre de la salle. Dès que Hodir est au milieu de la pièce (voir le diagramme) et que les PNJ ont été libérées une première fois, les dps peuvent aller sur Hodir. Toutes les classes de dps peuvent aller sur Hodir dès que le tank est dessus. 

En plaçant Hodir de cette manière, on oblige cependant tout le raid à être dispersé.

![pull](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Hodir-Basic-Position-2048x1152.png)



On peut éventuellement décider de le placer plutôt dans un coin mais de ce fait les cacs n'auront plus de puits de lumière à portée

![coin](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Hodir-Corner-Position-2048x1152.png)

Les dps devront prendre les premières secondes du pull, et seulement quand Hodis seras tanké, pour libérer les glacons dps les PNJ dans leurs glacon avant que le tank ai pull le boss.

Le Positionnement au cours de ce combat n'est pas fixe :  Il faudra un raid uniformément répartie autour de Hodir au début a la fin, mais cela (et va) changer rapidement dès les buffs des pnj et les joueurs doivent se repositionner pour prendre leurs buff et augmenter leurs dégâts. 

Le raid doit être 100% flexible dans leurs mouvement sdurant le combat - le positionnement n'est pas important tant que chacun reste proche d'un healer et n'est pas touché par les glacons. Essayez d'être buffé le plus souvent possible svp.



Le combat contre Hodir peut être considéré comme un cycle de 1 minutes, définit par Gel instantané. 

On suivra donc l'algorithme suivant :

1) Briser la glace :
   Immédaitement après le pull ou après chaque Gel instantané, les dps distance doivent casser les blocs de glace des PNJ. Plus vite les blocs de glace sont cassés, plus rapidement les PNJ pourront revenir dans le combat, et avec eux leurs buff.

2) Coups gelés
   À partir du moment où tout les blocs de glace ont été brisés, les PNJ vont alors commencer à buffer et Hodir va gagner Coups gelés. C'est à ce moment que le heal va être le plus intensif :  éviter Froid Mordant et les blocs de glace qui tombe du ciel ! On claque les cd de survie à ce moment (pds, popo)
3) Dès que Coups gelés a expiré, tous les buffs doivent être dispos et on disposera d'environ 30 secondes avant un nouveau Gel instantané. Maximisez votre dps à ce moment svp, on lâche les cd offensifs, trinckets et autres et ofc la BL.
4) Après 50 secondes, Hodir va commencer a recast des Gel instantané. Il faut alors vous positionner sur une zone où un bloc de glace va tomber (mais pas trop près), une fois le bloc tombé, allez dessus et attendez la fin du Gel instantané.

Recommencer de 1 à 5 jusqu'à la mort du boss (facile donc)

-----



### Hard Mode

Pour réaliser le Hard Mode et obtenir des loots supplémentaires, il faut le tuer avant qu'il casse un des coffres qui se trouve dans la pièce et donc fournir un très gros DPS (donc obligation de maitriser la strat pour maximiser les buffs des pnj)

Au bout de 3 min (2 selon le ptr ?) il casse le premier coffre, il en resteras alors plus qu'un seul. 

------

### Vidéos

- https://youtu.be/3uwg7WgX7mY?t=1501
- https://youtu.be/kIqyG3msJWA?t=3485