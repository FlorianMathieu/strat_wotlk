# Leviathan des flammes

<img src="https://wow.zamimg.com/uploads/screenshots/normal/135044-l%C3%A9viathan-des-flammes.jpg" alt="Leviathan" style="zoom: 50%;" />



### Introduction

Combat assez original : il ne s'agit pas d'un boss fight classique mais d'un destruction derby.

Le raid sera répartie dans plusieurs véhicules qui ont chacun leur utilité, et que vous pourrrez choisir avant le début du raid :

- **Engins de siège** (tank)
- **Démolisseurs** (dps)
- **Moto** (support)

Dans un premier temps, nous allons jouer dans le véhicule qui est lié à notre rôle en raid

**Mais pourquoi tu veux m'empêcher de m'amuser, connard ?**

Et bien parce que votre véhicule prend l'ilv de votre stuff et que les engins de siège auront besoin de beaucoup de pv, donc on comptera sur nos tanks surstuff pour mener à bien cette mission, de même que nos dps bien stuff feront donc de plus gros dégâts. 

Si vous avez bien suivi, de ce fait, mieux vaut équiper tout le stuff de raid 25 que vous possèdez pour ce combat, même si la pièce n'est pas opti, que ça ne correspond pas à votre spécialisation... L'important est d'avoir le plus haut ilvl possible !





Par ailleurs, attention : il n'y a pas assez de véhicule pour tout le monde :  ***Les engins de siège et les démolisseurs prendront également un passager chacun.***

Ces passagers auront des compétence à utiliser pendant le combat, mais une seule personne dirigera le véhicule. 

Ce qui nous fera un total de 5 engins de chaque type  + 10 passagers (Ce qui fait donc 25, les mathématiques sont magiques)



Soyons clairs : en mode normal, il ne posera aucun problème. Néanmoins, il est necessaire de lire la suite car le réel objectif sera de maitrîser le combat afin de pouvoir le taper en HM au plus vite, alors restons concentrés svp.

>  En NM, il dispose de 70 millions de pv.  
>
> Il ne faut surtout pas le tanker, sinon votre véhicule va très vite éclater. Il faudra le kitter. (comme Supremus à BT).
> Toutes les 30 secondes, il change de cible en ciblant un  **engin de siège**. S'il n'y a pas au minimum deux **engins de sièges**, il ira sur un autre type de véhicule.
>
> De plus, vos véhicules prennent des dégâts mais ne peuvent pas être soignés.
>
> -  **Flame Jets** : 2000 dégâts de feu par seconde pendant 10 secondes dans les 50 mètres autour de Léviathan. Peut être interrompu.
>
> - **Battering Ram**: attaque en mêlée qui écrase la cible et pose un debuff sur la cible : dégâts encaissés augmentés de 100%.
>
> - **Gathering Speed**: buff périodique qui se stack rendant Léviathan de plus en plus rapide (5%à chaque buff). Il est dissipé quand les joueurs lance **Overload Circuit**.
>
> - Il lance également toutes les 2 secondes des roquettes sur les raids qui font environ 700 dégâts.
>
> - Il a quatre tourelles. Les passagers sur les démolisseurs peuvent envoyer leurs passagers sur Léviathan pour les détruire (des dps range). Elles ont environ 50k points de vie chacune. A la mort d'une tourelle le joueur canalise un sort pour dérégler le Léviathan. Une fois les 4 tourelles mortes, 2 joueurs sur Léviathan lance **Overload Circuit** qui immobilise le boss et lui font subir +50% de dégâts pendant 10 secondes. Cette action dissipe également tous les buffs de vitesse du boss.





Arrivé à un certain nombre de **Gathering Speed**, le kiting devient franchement difficile quand le boss avance 2 fois plus vite que les engins de siège.

 Pour éviter ça, il faudra régulièrement que 4 passagers des engins soient envoyés sur le dos du boss (les passagers se mettent dans la catapulte et le conducteur active le lancement) et dps les 4 tourelles (seulement 2 tourelles en Raid 10). 

Cette fois, ce sont les vrais compétences de chaque joueur qui seront utilisées. Attention, de gros dégats sont à prévoir sur ces quatre perso,donc il faudra si possible  lancer les 4 passagers au même moment et que ces 4 joueurs fassent un gros burst dps pour ne pas rester là plus de quelques secondes.

Une fois les 4 tourelles downs, le boss passe en surcharge pendant 10 secondes, prend **50% de dégâts en plus** et perd ses débuffs de vitesse. Les 4 joueurs sont alors expulsés sur le champ de bataille, ils seront récupérés par les motos qui pourront les soigner puis les refileront aux engins initiaux pour un nouveau cycle.

Deuxième gestion à optimiser : le dps des tourelles. La phase de faiblesse du boss devient indispensable pour faire descendre ses nombreux points de vie. 

Une solution  : on ne balance que deux joueurs à chaque phase, et on fait 2 équipes de deux joueurs. Au début du combat, deux démolisseurs prennent chacun deux passagers (un de chaque équipe). L'équipe 1 est balançée très vite. Les joueurs de l'équipe 2 sont encore dans le démolisseur et ramassent la pyrite. 

Lorsque la phase de faiblesse s'arrête, les joueurs de l'équipe 2 sont envoyés aussitôt et quelques secondes plus tard, les joueurs de l'équipe 1 auront été récupérés par les motos et ramenés à leur démolisseur pour qu'ils s'occupent de la pyrite, et ainsi de suite. Un joli roulement à effectuer. 

Prévoir de préférence des casters pouvant se healer : démo, druide équi, chaman elem, prêtre ombre, etc.




## TLDR 

### Que dois je faire pendant le combat ?



- **Si vous êtes tank** :
  - Les engins de siège vont jouer le rôle de kiteur à la manière de Supremus à BT : le boss va se déplacer lentement vers un engin de siège et changer de cible toutes les 30 secondes. L'engin de siège ciblé devra alors kiter le boss tout autour de la zone de combat. 
  - La première capacité des engins de siège est la **Bouffée de Vapeur** qui permet de se projeter 50 mètres vers l'avant, permettant de s'écarter du boss et ainsi de le kiter efficacement. 
  - La deuxième capacité est **Electro-Choc**, qui permet de CS le boss, car celui ci génère une **AOE** autour de lui qui fait très mal. Ce sera le rôle des autres **engins de siège** (ceux qui ne sont pas la cible du boss), lesquels devront poursuivre le boss et kicker l'aoe régulièrement. 
  - Enfin, la dernière capacité n'est utile que pendant la phase de trash initiale. 

- **Si vous êtes passager d'un tank :**
  -  Vous pouvez activer un **bouclier défensif** si le boss se rapproche trop
  - Vous pouvez utiliser une batterie de DCA pour faire éclater les tonneaux de pyrite en l'air (voir partie sur les démolisseurs) ou balancer des boulets de canon sur le boss - uniquement s'il n'y a rien d'autre à faire - .





- **Si vous êtes dps** :
  - Vous allez devoir... taper le boss. 
  - Les démolisseurs fonctionnent à base de conteneur de pyrite comme ressource pour activer les capacités. Ces tonneaux se trouvent sur le champ de bataille, en l'air au début : il faut que les passagers les ramènent au sol avec la DCA afin de les ramasser.
  - Il faudra lancer des tonneaux de pyrite sur le boss, occasionnant des gros dégâts et un dot se stackant 10 fois (Comme Malygos). 
  - Pensez à conserver un peu de pyrite tout le temps pour rafraîchir les dots.
  - Une deuxième capacité permet de faire des dégâts sur le boss (mais plus faibles) sans utiliser de pyrite. 
  - Enfin, il vous faudra envoyer vos passagers sur le boss !



- **Si vous êtes passager d'un dps**
  - Il vous faudra utiliser votre DCA pour ramener les tonneaux au sol, puis les ramasser avec leur capacité de lasso. 
  - Vous devrez également lancer une allumer les plaques de goudron posées par les motos (voir partie moto), occasionnant des dégâts au boss qui passe dessus.
  - Pour certains d'entre vous, il vous faudra vous ejecter sur le boss pour détruire ses tourelles afin de passer le Leviathan dans une phase de fragilité (voir stratégie complète).
  - Astuce : ramassez la pyrite quand votre réservoir est à 50%, histoire de ne jamais se retrouver à sec.



- **Si vous êtes heal (ou dps mais support)**
  - Vous devrez poser du goudron juste devant le boss, ce qui d'une part le ralentira (et aidera les tanks donc), et d'autre part lui occasionnera des dégâts conséquents s'il est enflammé par les passagers des démolisseurs.
  - Pensez à ramasser les copains copines qui se feront éjectert du Léviathan, vous pourrez également les soigner.
  - Restez éloignés du boss, votre moto a très peu de points de vie et risque de souffir avec les roquettes du boss. 

-------

## Vidéos

- [Stratégie Leviathan en français](https://www.youtube.com/watch?v=aKNfQQQGqUQ) 
- [Stratégie Leviathan en anglais](https://www.youtube.com/watch?v=JvraP9VCkyk)