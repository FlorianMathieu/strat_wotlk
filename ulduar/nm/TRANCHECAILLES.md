



![Razorscale](https://dungeon.guide/pictures/dungeonboss/razorscale.webp)

## Introduction

Lorsqu’on en a terminé avec le **Léviathan des Flammes** on a directement accès aux trois autres boss du « Siège d’Ulduar ». Tranchécaille se trouve juste un peu plus loin, il suffit pour cela de suivre le couloir et de descendre les quelques marches qui mènent à son Aire, sur la droite. Ce boss est optionnel, au passage.

Pour la petite histoire, Tranchécaille est en fait **Veranus**, le Wyrm de Thorim, que Loken a emmené à Ulduar après sa trahison. D’abord corrompue par Yogg-Saron et devenue folle, **Tranchécaille** s’est ensuite vue prise en main par Ignis sur ordre de Loken pour lui fondre du métal sur le corps et la renforcer en vue d’en faire la bête de combat effroyable qu’elle est désormais. 

------



### Caractéristiques du boss

- **Points de vie** : 3,555,975 (10) – 12,550,500 (25)
- **Berserk** : 10 minutes

### Stratégie

On débute la rencontre en parlant au **Commandant de l’expédition**. Le combat lui-même est relativement simple et repose sur deux phases : une phase air-sol et une phase sol-sol. En gros, on utilise des tourelles pour lui tirer dessus et la faire descendre, on la combat alors au sol, puis elle remonte dans les airs et ainsi de suite. À 50% de sa vie elle reste au sol de façon permanente. Après 10 minutes de combat, elle devient berzerk et dévore le raid (miam miam)

**1) Phase 1 : air-sol**

La phase air-sol est donc constituée de **deux mini-phases**:

- Lorsque Tranchécaille est en l’air, des machines-taupe vont sortir du sol que les tanks et les DPS devront gérer au plus vite. Dès qu’on voit une [**Sentinelle sombre-rune**](http://fr.wowhead.com/?npc=33846) il faut s’en occuper en priorité sous peine de la voir complètement brûler les soigneurs. On devra ensuite s’occuper de tout ce qui a de la mana, puis terminer par les autres mobs qui ne seront plus un problème.

- Au bout d’un moment, un message de raid annonce que **les Tourelles** sont prêtes. Un DPS distance devrait alors les utiliser pour faire venir Tranchécaille qui restera au sol paralysée pendant quelques secondes. Profitez-en pour utiliser vos **cooldowns** et la descendre un maximum. Tranchécaille se réveille, retourne dans les airs et on reprend en gérant les taupes et les sentinelles. Notez qu’il y a 2 tourelles en mode 10 et que vous devrez utiliser les 4 disponibles en 25.

**Le but est de descendre Tranchécaille à 75% dans la première mini-phase, puis à 50% lors de la seconde** pour la passer définitivement dans la deuxième phase au sol. Si jamais nous n’y parvenons pas, nos chances de survie seront faibles dans cette rencontre qui constitue un peu, par ailleurs, une vérification du niveau du DPS du raid dans Ulduar, un peu comme Le Recousu à Naxxramas.

**Capacités de Tranchécaille en phase 1 :**

1. **Flamme dévorante :** de 5088 à 5912 (10) ou 7,863 à 9,137 (25) points de dégâts de Feu infligés toutes les 2 sec.
2. **Boule de feu** : de 6660 à 7740 points de dégâts de Feu (9,713 à 11,287 en 25).

**2) Phase 2 : sol-sol**

Arrivée à 50% de sa vie, Tranchécaille restera définitivement au sol ce qui débutera la phase 2. Comme Tranchécaille laisse une trainée de feu très dangereuse sur le sol derrière elle, **le tank se déplace à reculons** sur la ligne circulaire du sol au milieu de l’Aire. D’une manière générale, l’idée est d’**interchanger les tanks** sur le boss à intervalles réguliers afin de leur éviter de recevoir plus de 2 debuffs de **Fusion de l’armure** (si Tranchécaille résiste à un taunt, cela laisse un peu de marge de manoeuvre). Si les DPS distance et les soigneurs sont placés au centre de la zone circulaire, il faudra alors que les tanks prennent bien soin de **ne jamais diriger Tranchécaille vers le centre** lorsqu’ils la taunt : si jamais tout ce petit monde fragile se prend un Souffle de flammes dans le visage, on peut compter sur plusieurs one shot et un wipe probable (et un monkey d'or)

**Capacités de Tranchécaille en phase 2 :**

1. **Fusion de l’armure** : réduit l’armure ainsi que les vitesses d’attaque et de déplacement de 20% pendant 20 sec. Stackable 5 fois. Après 5 debuffs le tank est assommé pendant plusieurs secondes.
2. **Rafale de flammes** : augmente les dégâts de Feu subis par un ennemi de 1000 pendant 1 minute (1500 en 25). Stackable 99 fois (déso)
3. **Frappe des ailes** : repousse les ennemis se trouvant dans un rayon de 35m autour du lanceur.
4. **Souffle de flammes** : inflige de 13,125 à 16,875 (25 : 17,500 à 22,500) points de dégâts de Feu aux ennemis qui se trouvent dans un cône devant Tranchécaille.
5. **Flamme dévorante** : crache une bombe de lave sur un ennemi et lui inflige 5088 à 5912 points de dégâts de Feu (7,863 à 9,137 en 25), puis autant toutes les secondes pendant deux secondes à toute personne se trouvant à une portée de 8m de l’impact.

----------

###  Vidéos 

- https://www.youtube.com/watch?v=07p5orCvCas
- https://youtu.be/kIqyG3msJWA?t=442