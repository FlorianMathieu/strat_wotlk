## XT-OO2

![XT-002](https://dungeon.guide/pictures/dungeonboss/xt002deconstructor.webp)



### Caractéristiques du boss

- **Points de vie** : 5,250 000 (10) – 18 000 000 (25)
- **Berserk** : 10 minutes

---------



### Capacités

- ***Bombe à gravité***
  Charge la cible avec une énergie noir, ce qui les explose et fait aussi le sort Bombe à gravité, ce qui va puller les alliés proche et leur infligés 19000 à 21000 points de dégâts. Il faut simplement s'écarter des autres quand on as le débuff.

- ***Colère assourdissante***
  Inflige 10% de dégâts chaque secondes pendant 12 secondes. Ce sort est casté toutes les minutes.

- ***Bombe de lumière***
  Charge la cible avec une énergie divine, la cible inflige alors 2500 points de dégâts aux alliées proche pendant 9 secondes.

-----------

### Déroulé du combat

XT-002 est un combat en 2 phases qui alterne tous les 25% de vie du boss en moins.

P1 : Phase qui consiste à lui déboiter la tronche en monocible
P2 : Phase de DPS du coeur et apparition des Adds.

-----------

### Stratégie

Personne ne doit bouger excepté les CàC si nécessaire.
La BL sera lancée sur la première apparition du coeur.

**Tank**

> Main Tank
>
> - Le pull doit se faire quand XT-002 est immobile au centre des escaliers et fait ses mouvements de gym. Là vous devez le pull au mur protecteur/bubulle/supression de la douleur.
> - Gardez ses trinkets/CD d'urgence pour les retours d'XT-002 en phase 1 et pour le cas ou vous auriez la bombe.
> - Pensez à prendre au début du combat une potion d'armure juste avant de pull
> - Un pull de 40k pv est une bonne base pour se prendre les coups envoyés



> Off-Tank
>
> - Chaque OT doit se répartir une zone pour récupérer les élites (gros robots) qui popent aux 4 coins de la map
> - Les Cd d'urgence doivent être garder au moment ou vous avez une bombe sur vous en plus des adds, mais également pour la dernière phase du coeur car c'est à ce moment là que vous aurez beaucoup d'adds sur vous et donc beaucoup de dégats.
> - les adds sont stunables


**Heals**

> - La phase pour remonter le raid apres le tremblement de terre est intense.
> - Pensez à bien envoyez sur la personne ayant la bombe.


**DPS Distance**

> - Chaque dps ne bouge quasiment pas. ils continuent de dps le coeur tant que les adds non élites ne sont pas à porté. (pourquoi ne pas bouger ? tout simplement pour gagner en temps de dps et surtout ne pas perdre le placement et donc gagner en efficacité de heal)
> - Dés que les adds non élites sont a portée, alors là, chacun lance sa plus grosse AOE.
> -  Lors du focus des Adds, la priorité reste de DPS les bombes pour qu'elles n'arrivent pas dans le raid. les dps répartis par zone doivent se concentrer ensemble pour la tomber.
> -  Garder ses trinkets pour les phases de burst du coeur
> -  Une fois que tous les adds non élites sont downs, on repasse sur le boss et ainsi de suite.


**DPS CàC**

> - Ne pas hésitez à regarder où se trouve les adds pour aider au besoin dans telle ou telle zone.



-------



### Placement

- P1 :

![P1_XT](https://www.warcrafttavern.com/wp-content/uploads/2022/11/XT-002-Deconstructor-Boss-Phase-Raid-Guide-2048x1152.png)



- P2 :

![P2_XT](https://www.warcrafttavern.com/wp-content/uploads/2022/11/XT-002-deconstructor-add-phase-raid-guide-2048x1152.png)

--------

### IMPORTANT

- Lorsqu'un dps se prend le tremblement + la bombe, ne pas hésitez à mettre un CD d'urgence sur lui.
- Le coeur prend le double des dégâts par rapport à XT et chaque dégat sera transmis au boss quand il reprendra sa forme initiale.
- La phase coeur dure 20 secondes.
- Ne pas détruire le coeur si on ne veut pas passer en **HARD MODE** !!!



-----

### HARD MODE

- Pour activer le Hard Mode, il faut tuer son coeur pendant la phase "Exposer son coeur".
  Il obtiendras alors le buff : ***Bris du coeur*** , ce qui va augmenter ses dégâts infligés et sa vie de 25%.

- Il va aussi faire pop des Chargé d'électricité statique, ce qui inflige 500 points de dégâts de nature à tous les ennemis qui se trouvent à moins de 500 yards. Tick à 800 en mode 10 et 1500 en mode 25.

Pour réussir à le tuer en mode HM, il faudra beaucoup de dps (et vu les logs du ptr, personne n'y arrive :D )

-----



### Vidéos 

- https://youtu.be/kIqyG3msJWA?t=739
- https://www.youtube.com/watch?v=CSvK0GFuK0c