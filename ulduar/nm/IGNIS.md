# Ignis

<img src="https://wow.zamimg.com/uploads/screenshots/normal/149405-ignis-the-furnace-master.jpg" alt="Ignis" style="zoom: 50%;" />



### Introduction

- Avant de pouvoir faire face à Ignis, il faudra gérer **plusieurs trashs** pas très sympa.

- Pour passer ceux qui montent la garde devant le passage, on place un tank à chaque coin du couloir d’accès à la Fournaise et on demande à leur soigneur de se placer à la limite de portée le long du mur perpendiculaire (dans le couloir principal). Ces trashmobs particulièrement vache ont **la facheuse manie d’étourdir tout le monde** à courte portée et à taper très, très fort sur les tanks.

- Pour les trashs suivants, sortes de cyclônes humanoïdes, tout le monde doit faire attention à les garder bien en face pour pouvoir **les éviter rapidement**. Si vous avez du mal à les anticiper, attendez vous à être bumpé plusieurs fois de suite, incapable d’agir en voyant votre vie fondre comme beurre au soleil…

  

  ### Caractéristiques d’Ignis le maître de la Fournaise :

  - **Points de vie** : 5,578,000 (10) – 19,523,000 (25)
  - **Enrage** : non
  
  ### Capacités du boss :

  1. **Flots de flammes** : Éruption de geysers de flammes qui infligent 5655 à 6345 points de dégâts de Feu, projettent les cibles dans les airs et interrompent tous les sorts en cours d’incantation pendant 6 sec. De plus, les cibles subissent 1000 points de dégâts de Feu par seconde pendant 6 sec.
  2. **Brûlure** : Un jet de flammes qui brûle tous les joueurs se trouvant à moins de 30 mètres devant Ignis, infligeant 2357 à 2643 points de dégâts de Feu toutes les demi-secondes pendant 3 sec. Le sol prend feu et inflige 1885 à 2115 points de dégâts de Feu supplémentaires toutes les secondes aux joueurs se trouvant à moins de 13 mètres. Les assemblages de fer situés dans ce rayon commencent à chauffer et finiront par entrer en fusion.
  3. **Assemblage de fer** : Un add de fer qui pop et aide Ignis. Chaque assemblage de fer qui pop donne le buff *Force du créateur* à Ignis tant qu’il est en vie, stackable jusqu’à 99 fois.
  4. **Force du créateur** : augmente les dégats d’ignis de 20%.
  5. **Marmite de scories** : Charge un joueur aléatoire, l’attrape et le lance dans sa marmite de scories. Le joueur subit 4500 points de dégâts de Feu toutes les secondes pendant 10 sec. S’il survit, il est imprégné des scories magiques, ce qui augmente sa hâte de 100% pendant 10 sec.
  
  

  -----------

  

  

  ### Stratégie

  ![ignis-flotdeflammes](https://ccelenn.files.wordpress.com/2009/07/ignis-flotdeflammes1.jpg?w=300&h=258)

  On commence classique :  avec un tank qui tank et des DPS qui dps.
  Seuls les casters devront bien prendre soin d’interrompre leur lancement de sort lorsqu’Ignis incante son ***Flot de Flammes*** (voir image ci dessus ) sous peine d’être bloqué pendant les 6 secondes suivantes.
  
  

  

  

  ![ignis_brulures](https://ccelenn.files.wordpress.com/2009/07/ignis_brulures1.jpg?w=300&h=300)

  

  

  Un **pull au détournement** permettra de placer tout de suite correctement Ignis près de l’un des bassin, directement au coin de mur qui touche le bassin. Un offtank sur le qui-vive sera près à **intercepter chaque assemblage de fer** qui popera (attendez-vous à ce que les adds foncent directement sur les heals raids ) et l’emmènera suivre son processus de destruction (voir plus bas).

  Au fur et à mesure que le combat avance, le tank bouge le boss pour le sortir des *Brûlures* et s’assure qu’il reste toujours dos au raid (qui est de préférence placé en plein centre de la salle, sur les lignes séparant les deux bassins d’eau). C’est tout pour lui, le plus dur devra être réalisé par les soigneurs et l’offtank. 

  

  Voir ci-après le positionnement idéal :

  [![ignis_ulduar_boss_strategie](https://ccelenn.files.wordpress.com/2009/07/ignis_ulduar_boss_strategie1.jpg?w=490)](https://ccelenn.files.wordpress.com/2009/07/ignis_ulduar_boss_strategie1.jpg)

  **L’offtank doit intercepter les adds** un par un et permettre leur destruction. Pour se faire, il n’y a qu’un seul moyen :
  
  1. Les faire marcher sur les *Brûlures* (ils recoivent alors le buff « *[Chaleur](http://fr.wowhead.com/?spell=62343)* » qui augmente leur vitesse de déplacement ainsi que leur hâte, stackable 20 fois).
  2. Lorsqu’ils ont 10 buffs chaleur, les adds recoivent un nouveau buff nommé « *[En fusion](http://fr.wowhead.com/?spell=62373)*« , qui augmente leur hâte et leurs dégats en plus de faire une AOE de feu rapprochée (7m).
  3. À ce moment l’offtank emmène l’add dans un bassin d’eau, ce qui paralyse l’add 20 secondes en le rendant « *[Fragile](http://fr.wowhead.com/?spell=62382)*« .
  4. Un dps distance doit alors lui infliger un dégat de 5000 pts en une seule fois, ce qui le détruit, provoquant également une explosion de proximité de 18,850 à 21,150 pts, non mitigés (7m).

  Lorsque l’**assemblage de fer** est détruit, Ignis perd [son buff](http://fr.wowhead.com/?spell=64473) aux dégats et le(s) soigneur(s) assigné(s) au tank souffle(nt) un peu. Assurez-vous qu’il n’y ait jamais plus de deux assemblages de fer en action au même moment… et de préférence un seul le moins longtemps possible.

  ![ignis_assemblagesdefer](https://ccelenn.files.wordpress.com/2009/07/ignis_assemblagesdefer1.jpg?w=490)

  ### Soins

  **En 10**, 1 soin sur le MT1, un autre sur le MT2, et sur le raid, attribution classique. 
  
  **En 25**, pareil mais avec une nuance : si vous êtes assigné au soin de raid, restez-y et **faites confiance aux autres heals** pour gérer leurs propres assignations. 
  
  Si vous êtes sur le soin des tanks :  ne cessez pas de caster et ne vous refusez aucun **overheal** si vous le pouvez car il y aura forcément des interruptions ou des situations pendant lesquelles vous serez livré à vous même, tout seul, pour prendre soin de votre tank.

---------

### Vidéos :

- https://www.youtube.com/watch?v=Gra4eY8slDk
- https://www.youtube.com/watch?v=j7tQs-yNc2k