## Assemblée de fer



![Assemblée](https://www.warcrafttavern.com/wp-content/uploads/2022/10/Council-2-2048x1152.jpg)

### Introduction

La rencontre est un affrontement de 3 boss : 

- Brise-Acier le plus grand
- Maître des Runes Molgeim le moyen
-  Mande-Foudre Brundirle le petit. 
- 10M de points de vie (chacun) à 25 et 3M à 10
- Dès que l'un des trois meurt, les deux autres gagnent +25% de dégâts, une capacité supplémentaire et sont soignés intégralement. 
- Enrage 10 minutes.

-------

### Capacités :

#### ***Brise-Acier***

- Il tape à 25k (en mode 25).
- ***Si aucun boss n'est mort auparavant :***
  - Il lance Coup de poing fusion sur un timer compris entre 10 et 30 secondes. Cette attaque puissante ajoute un DoT de 8000 dégâts nature par seconde pendant 4 secondes (6500 en mode 10). 
  - Si le boss est placé dans une Rune de puissance , il tapera plus fort de +50%. Il est donc important de ne pas le faire entrer dans une telle zone.
  - Il possède une aura Haute Tension qui fait 3000 dégâts nature toutes les 3 secondes (1500 en mode 10). Elle disparaît quand il meurt.

- ***Si un boss est déjà mort :***
  - En plus des capacités ci-dessus, Brise-Acier gagne ***Perturbation statique***, une aura qui fait 7500 dégâts nature (5000 en mode 10) et donne un debuff qui augmente les dégâts natures de 50% pendant 20 secondes. Cette capacité oblige un switch régulier du tank de Brise-Acier en mode hard (s'il n'est pas tué en premier).

***Si deux boss sont déjà morts :***

- En plus des capacités ci-dessuss, à chaque fois que Brise-Acier tue quelqu'un il obtient un buff dont les effets s'additionnent : Supercharge (+25% de dégâts).
- Brise-Acier lance un buff sur une cible, Puissance accablante. Cette cible fait +200% de dégâts pendant une minute puis subit Fusion (la cible meurt et cause 9k dégâts nature à tous les joueurs dans les 15 mètres autour d'elle).





#### ***Maître des Runes Molgeim***

- Il tape à 9k.
- Maître des Runes Molgeim lance Rune de puissance périodiquement sur une cible NPC. Elle reste au sol pendant une minute et il n'y a que quelques secondes entre la fin d'une rune et le lancement d'une suivante. 
- Si le boss ou n'importe quel joueur est placé dans une Rune de puissance, il aura ses dégâts augmentés de +50%
- Barrière runique : un bouclier qui réduit les dégâts physiques de 50% et inflige 2000 dégâts d'arcane aux attaquants.

- ***Si un boss est déjà mort :***
  En plus des capacités ci-dessus, il lance une Rune de mort au sol, ciblé sur un joueur au hasard. Cette rune fait 10000 dégats par seconde (7000 en mode 10) dans les 13m autour de la rune.
-  Il n'y a aucun délai entre les dégâts et le cast de la rune. Cela demande donc une bonne réactivité pour en sortir rapidement.
- Il lance périodiquement Bouclier des runes sur lui même : une fois qu'il a subi 50.000 dégâts, il augmentera son dps de 50% pour 15 secondes. Ce shield peut être dissipée ou volée.

***Si deux boss sont déjà morts :***

- Quand Maître des Runes Molgeim est le dernier boss, il utilise Rune d'invocation, ce qui fait apparaître une zone de vide d'où sortent des Elémentaires qui font des dégâts de zone : Souffle foudroyant: fait 14138 à 15862 dégâts nature dans les 30 yards (9425 à 10575 dégâts à 10 joueurs) et l'élémentaire est détruit après le blast.



#### ***Mande-Foudre Brundir***

- Lance des chaines d'éclair : 6-9k dégâts . Ce sort peut être interrompu et ralenti à l'aide de malédiction des langages par exemple.
- Régulièrement, il fait un emote "PEASANT CROSSES THE LINE" en même temps qu'il lance un sort pendant 10 secondes appelé Surcharge : c'est un dégât de zone qui fait 25000 dégâts nature dans les 30m autour de lui. 
- Il est donc vital que le raid s'éloigne de  Brundir rapidement une fois que l'emote est lancé (ce sort ne peut pas être interrompu).

***Si un boss est déjà mort :***

- Eclair tourbillonnant : Des boules d'éclair sont lancées sur les joueurs au hasard pendant 5 secondes.
-  Chaque boule fait des dégâts nature (4713 à 5287 à 10,  6598 à 7402 à 25). Le sort peut être CS.

***Si deux boss sont déjà morts :***

- Quand Mande-Foudre Brundir est le dernier boss, il s'envole avec des éclairs tout autour de lui et ne peut plus être étourdi ou provoqué. 
- Ces vrilles d'éclair font 4000 dégâts nature toutes les secondes à tous les joueurs autour du caster. A la manière de Sartura dans AQ40, le boss suit une cible et change régulièrement. 
- De plus, il gagne le buff Stormshield, ce qui fait 250 points de dégâts aux attaquant et le rend immune aux stun. Son aggro est reset à chaque atterrissage.

--------



### Stratégie

#### Phase 1 : Les 3 boss sont vivants.

Le plus simple est de descendre Brise-Acier en premier, c'est lui qui fait le plus de dégâts sur le tank surtout lors des Coup de poing fusion. 

 Le DK est certainement le meilleur tank possible.

Les deux autres boss doivent être offtankés :

- Mande-Foudre Brundir doit être tanké à plus de 30m du raid pour ne pas subir Surcharge.
-  Néanmoins, lorsque Mande-Foudre Brundir reçoit la Rune de puissance, il est indispensable de l'interrompre pour être déplacé. Un guerrier est donc le meilleur tank possible ici.
- Si vous placez Runemaster à côté de Mande-Foudre Brundir les Rune de puissance ont 2 chances sur 3 d'être positionnées au même endroit (à l'endroit où sont tankés ces deux boss). Il suffira alors de les bouger tous les deux à un autre endroit pour qu'ils ne bénéficient pas de la rune pendant que le raid et surtout les dps range pourront se placer dans la Rune de puissance et bénéficier des +50% dégâts.
- Tout le dps doit être concentré sur Brise-Acier. Il ne sert à rien de faire des dégâts sur les autres puisqu'une fois qu'il sera down, il soignera les deux autres.



#### Phase 2 : 2 boss sont vivants

Il ne reste plus que Runemaster Moldeim et Mande-Foudre Brundir en vie, il est plus facile de tuer Runemaster Moldeim.

Dans cette phase, les deux boss doivent être tanké idéalement à 30m l'un de l'autre.

Si la Rune de puissance est lancée sur Runemaster Moldeim personne ne bouge, les casters viennent au corps à corps pour bénéficier de la rune. Le boss ne fait pas assez de dégâts pour avoir besoin de le faire bouger et tous les dps (y compris les corps à corps) gagnent ainsi +50% dégâts.

Si la Rune de puissance est lancée sur Mande-Foudre Brundir, les deux tanks intervertissent leur position et le raid suit Runemaster Moldeim et se place dans la rune comme précédemment.

La Rune de mort est particulièrement dévastatrice. Sans buff accélération de course, il est difficile de sortir de sa zone d'effet sans prendre 12 à 18k dégâts (les 6000 dégâts sont infligés immédiatement une fois la rune lancée).

 Les joueurs du raid doivent donc être espacés à plus de 13m les uns des autres autant que possible et il faudra call la personne ciblée. 

Attention en particulier si Rune de mort est envoyée sur un joueur du groupe qui bénéficie de la Rune de puissance.

Le guerrier chargé de Mande-Foudre Brundir devra interrompre Lightning Whirl. 

Si la Rune de mort est envoyé sur notre tank, il est possible qu'il ne puisse pas revenir au corps à corps et que Mande-Foudre Brundir continue à caster dans la Rune de mort. Dans cette situation, un Chevalier de la mort de devra lancer une Strangulation.





#### Phase 3 : Le dernier boss


Mande-Foudre Brundir est le boss le plus facile à terminer en dernier (donc le mode normal)

Il possède maintenant la capacité de s'envoler avec des éclairs tout autour de lui : chaque tic fait 4000 dégâts nature toutes les secondes à tous les joueurs autour du caster. 

A la manière de Sartura dans AQ40, le boss suit une cible et change régulièrement. 

Son aggro est reset à chaque atterrissage : nos tanks devront  enchainer les taunts pour le maintenir à une place "stable" pendant que les dps range lui font des dégâts.

Une fois que le boss atterrit, il faut le maintenir sous silence ou stun afin qu'il ne puisse plus s'envoler de nouveau.



---------



### Hard Mode

Pour faire le Hard Mode et obtenir les loots qui correspondent (notamment le disque de données pour accéder à Algalon), il faut tuer Brise-acier en dernier.





-----------

### Vidéos

- https://youtu.be/kIqyG3msJWA?t=974
- https://www.youtube.com/watch?v=vBPyfOnf18g (pov tank)
- https://youtu.be/3uwg7WgX7mY?t=466

