## Thorim



![Thorim](https://www.warcrafttavern.com/wp-content/uploads/2022/10/Lightning-Man-2048x1152.jpg)

### Introduction

Thorim, le fameux camarade de la suite de quête dans les pics foudroyés (dont Tranchécaille était la monture, suivez un peu ! ) est un des **Quatres gardiens d'Ulduar**. Après avoir été enlevé par **Lokken** avec son dragon, nous voilà devant une fausse copie de sa femme, Sif, prêt à nous arrêter.

Le combat contre Thorim se déroule en deux phases distinctes. Lors de la première, la moitié du raid affrontera des adds dans l'arène centrale pendant que l'autre moitié se fraiera un chemin jusqu'au balcon de Thorim.

---------

### Stratégie

### **Phase 1 de l'affrontement contre Thorim à Ulduar**

Lors de cette phase, on va séparer le raid en deux groupes:

- Le premier va se déplacer dans des couloirs jusqu'à atteindre Thorim
-  le second va rester dans l'arène pour affronter des vagues d'ennemis.

Au niveau de la **composition des groupes** :

- On peut laisser un peu plus de personnes partir dans les couloirs. 
- Dans l'arène, prévoir de garder des classes qui sont excellentes pour taper en zone. Il n'y aura pas de mini-boss, à la différence des couloirs. Évidemment il faut du tank et du heal dans les deux groupes.

**Pour le groupe dans l'arène** :

- Restez assez proche les uns des autres pour faciliter le travail de votre tank tout en vous écartant un peu pour éviter le plus possible de subir trop de dégâts des différents sorts de Thorim. 
- Évitez le plus possible d'être sur les bords de la salle pour ne pas prendre les dégâts supplémentaires de l'activation d'orbes électriques.

Vous allez rencontrer plusieurs types d'adds. Tuez-les dans cet ordre.

- **Porteguerre sombre-rune :** Il faut les tuer rapidement ou en contrôler un mentalement pour le buff de hâte.
- **Acolyte sombre-rune :** Interrompez-les le plus possible.
- **Évocateur sombre-rune :** Interrompez-les le plus possible.
- **Champion sombre-rune** **:** Attention à ses charges et tourbillons.
- **Roturier sombre-rune :** Rien de particulier, gardez bien l'aggro.

![couloir_1](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Thorim-Giant-2-2048x1152.jpg)

**Pour le groupe dans le couloir qui doit remonter jusqu'à Thorim** :

Dans la première salle, gardez constamment votre regard sur le **golem de fer** au fond. Il va lancer des frappes qui vont rendre la moitié de la salle impraticable. Il lèvera son bras du côté de la salle dont il faut s'écarter.

![couloir_2](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Thorim-Giant-One-2048x1152.jpg)

Dans cette phase, tuez toujours les **Acolytes** d'abord. Tankez-les **Gardes** dos au raid. Quand vous arrivez au **golem de fer**, tankez-le dos au raid, évitez les frappes et restez bien derrière lui dès qu'il charge.

Une fois le golem de fer tué, montez les escaliers et vous tomberez contre un **Géant**.

Une fois dans la dernière salle, dans laquelle vous trouverez Thorim, **ne marchez pas dans les grands cercles au sol**. **Si on décide d'activer le hardmode, il faudra engager Thorim avant que Sif ne parte**. Sinon, attendre que Sif parte.



![couloir_3](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Thorim-Trap-Hallway-2048x1152.jpg)

----

### **Phase 2 de l'affrontement contre Thorim à Ulduar**

Une fois que vous avez réussi à atteindre Thorim, sautez du balcon. La phase 2 commence.

Dans cette phase, il faudra tanker activement **Thorim**. Il sera seul et n'invoquera plus d'adds. **Restez écartés les uns des autres** pour éviter les dégâts de **Chaînes d'éclair,** on peut même envisager un placement comme sur **Kel'Thuzad** si problème d'espace.

![positionnement](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Thorim-Fight-Blizzard-2048x1152.png)



Quand Thorim lance **Frappe déséquilibrante** sur le MT, changer de tank le temps que le débuff disparaisse.

Comme en phase 1, Thorim va changer une des orbes sur le côté de la salle (aussi appelées pylône). 

Ne pas rester proche de cet orbe et **surtout** ne pas rester entre **Thorim et l'orbe** quand il lance **Charge de foudre**.

**En Harmode**, il faudra gérer **Sif** en plus de Thorim. Elle va infliger des dégâts de givre supplémentaires pendant la phase. Ne pas trainer dans le **Blizzard** et **écartez-vous** d'elle lorsqu'elle va lancer la **Nova de givre.**

-------

### Hard Mode

Terminez la phase 1 assez vite pour pouvoir accéder au hardmode.

-------

### Vidéos

- https://youtu.be/kIqyG3msJWA?t=2807
- https://youtu.be/3uwg7WgX7mY?t=2100