## Auriaya

![Auriaya](https://www.warcrafttavern.com/wp-content/uploads/2022/10/Catlady-2-2048x1152.jpg)

### Introduction


Les trash juste avant Auriaya sont un peu chiants:

Par pack de deux, il faudra les les écarter. Au bout d'environ 10 à 20 secondes, une boule électrique va partir de l'un pour aller vers l'autre.Il faut dps cette boule avant qu'elle l'atteigne, sinon, le mob obtiendras le buff "Etat de supercharge", ce qui va augmenter ses dégâts infligés de 50% et permet à ses attaque de libérer de l'energie electrique.

Il faudra également les tuer en même temps (Stalagg et Feugen like), sinon, celui qui reste envie aura le buff Décharge Vengeresse qui augmente les dégâts infligés de 100% ainsi que la vitesse d'attaque de 50%.

Nous voilà donc devant la vieille dame aux chats



![dame_aux_chats](http://laterredabord.fr/img46/o35.png)



--------

### Capacités 

#### Auriaya

- Hurlement sonore
  Une onde de choc sonore qui inflige 74000 à 86000 points de dégâts physiques à tous les ennemis se trouvant sur son chemin. Les dégâts de l'onde de choc sont partagés entre toutes les cibles touchées : ***ON RESTE PACKÉS MERCI***.

- Déflagration du factionnaire
  Une violente déflagration qui inflige 3500 points de dégâts d'Ombre et augmente les dégâts d'Ombre subis de 100% pendant 5 sec.. Il est stackable 5 fois.

- Hurlement terrifiant
  Le hurlement sème la peur dans les cœurs des ennemis proches, ce qui les fait fuir, horrifiés, pendant 5 sec. ***GROS TOTEM OBLIGATOIRE***.

- Invocation d'un gardien grouillant
  Elle fait pop une vague de panther (environ 20), ils ont environ 13k pv en 25 et 3k en mode 10.



#### Panther Adds - Gardiens du sanctuaire

- Il y a 4 adds qui tourne dans la salle avec Auriaya en 25 et deux en mode 10.

- Force de la meute
  La présence d'autres membres de la meute augmente les dégâts infligés par tous les membres de la meute se trouvant à moins de 10 mètres de 50%. (Stackable, il faudra donc séparer les pantères pour éviter ce buff)

- Bond Sauvage
  Un bond sauvage qui inflige 5550 à 6450 points de dégâts physiques + 3700 à 4300 points de dégâts physiques toutes les 1 sec. pendant 5 sec.



#### Adds - Défenseur Farouche

- Ces adds pop 1 minutes après le début du combat. A noter que quand elle pop elles ont 9 stacks d'Essence farouche.

- Essence farouche
  Chaque essence augmente les dégâts infligés par le défenseur farouche de 50%. Le défenseur farouche peut se ressusciter au prix d'une de ses Essences farouches. (9 stacks pour les 9 vies des chats toussa toussa)

- Bond farouche
  Le défenseur farouche bondit sur la cible, l'étourdit pendant 4 sec et lui inflige des dégâts d'Ombre toutes les 1 sec.

- Essence farouche suintante
  L'Essence suintante du défenseur farouche inflige 6500 points de dégâts d'Ombre toutes les 1 sec. Il s'agit d'une attaque de zone infligés par le Défenseur Farouche quand il spawn et quand il meurt.

- Ruée farouche
  Charge un ennemi, lui inflige des dégâts d'Ombre et interrompt les incantations de sorts pendant 6 sec.

-------



### Stratégie 


Il faut avoir au moins 3 tanks de préférence (même si un tank improvisé peut tanker Auriaya).

Le principe est simple, les 2 OT vont chacun pull deux des chats et les tanker dans leurs coins. 

![pre_pull](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Auriaya-Pre-Pull-2048x1152.png)

Au pré pull, mieux vaut être regroupé de la sorte, puis pull à l'aoe du dk ou au bouclier du paladin.



![pull_dk](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Auriaya-Pre-Pull-Death-and-Decay-2048x1152.png)



Le MT va tanker Auriaya dans son coin aussi. On va devoir donc focus un des 4 chats puis un autre ensuite etc.



![fight](https://www.warcrafttavern.com/wp-content/uploads/2022/12/Auriaya-Fight-1-1-2048x1152.png)





Une fois les chats morts : Les Défenseurs Farouches vont apparaitre (au bout d'une mintute), on les off tank pendant que les dps s'occupent du boss. Inutile de les tuer sauf pour le HF puisqu'il faudra le faire 9 fois...

Détail tout de même important : à chaque mort, les défenseurs font moins de dégats quands ils reviennent.



-----------

### Hard Mode



Pas de HM...



-------

### Vidéos

- https://youtu.be/kIqyG3msJWA?t=1505
- https://youtu.be/3uwg7WgX7mY?t=1189



